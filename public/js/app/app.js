//inject angular file upload directives and services.
var scaffold = scaffold || {};

scaffold.Controllers = angular.module('scaffold.controllers', []);
scaffold.Services = angular.module('scaffold.services', []);

var config = function($routeProvider) {

    $routeProvider
        .when('/', {
            templateUrl: '/public/views/index.html',
            controller: 'PersonCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });
}

angular
    .module('scaffold', ['ngRoute','scaffold.controllers','scaffold.services'])
    .config(config)
