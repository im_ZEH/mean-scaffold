'use strict';

module.exports = function(mongoose){
	var Person = new mongoose.Schema({
		name: String,
		age: Number
	});

	mongoose.model('Person', Person);
}
