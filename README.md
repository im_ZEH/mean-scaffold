# MEAN Stack Scaffolding

This is a repo for a starter application for a Single Page MEAN Stack application. Just download and install and you have a good foundation for building application. 

## Installation
1. Download the repository
2. Install npm modules: `npm install`
3. Install bower dependencies `bower install`
4. Start up the server: `node index.js`
5. View in browser at http://localhost:3000