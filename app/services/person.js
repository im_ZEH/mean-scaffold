'use strict';

var sampleDao = require('../daos/sampleDao');

function Sample() {
    this.sampleDao = sampleDao;
}

Sample.prototype.getPerson = function(next) {
    sampleDao.getPerson(next);
}

Sample.prototype.getSpecific = function(id, next) {
    sampleDao.getSpecific(id, next);
}

Sample.prototype.savePerson = function(data, next) {
    sampleDao.savePerson(data, next);
}

Sample.prototype.updatePerson = function(id, data, next) {
    sampleDao.updatePerson(id, data, next);
}

Sample.prototype.deletePerson = function(id, next) {
    sampleDao.deletePerson(id, next);
}

exports.Sample = Sample;
