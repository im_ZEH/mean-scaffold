'use strict';

var sampleService = require('../services/person').Sample;
var sample = new sampleService();

var cb = require('./../utils/callback');

exports.getPerson = function onRequest(req, res) {
    sample.getPerson(cb.setupResponseCallback(res));
};

exports.getSpecific = function onRequest(req, res) {
    sample.getSpecific(req.params.id, cb.setupResponseCallback(res));
};

exports.savePerson = function onRequest(req, res) {
    sample.savePerson(req.body, cb.setupResponseCallback(res));
};

exports.updatePerson = function onRequest(req, res) {
    sample.updatePerson(req.params.id, req.body, cb.setupResponseCallback(res));
};

exports.deletePerson = function onRequest(req, res) {
    sample.deletePerson(req.params.id, cb.setupResponseCallback(res));
};